#include <iostream>
#include <stdexcept>

#include <vexcl/vexcl.hpp>

#include <boost/progress.hpp>
#include <boost/numeric/odeint.hpp>
#include <boost/numeric/odeint/external/vexcl/vexcl.hpp>

#include <H5Cpp.h>

#include "precondition.hpp"
#include "config.hpp"
#include "saver.hpp"
#include "dem_system.hpp"

#ifdef SINGLE_PRECISION
typedef float real;
#else
typedef double real;
#endif

typedef typename vex::cl_vector_of<real, 2>::type real2;

namespace domain {
    real2 lo;
    real2 hi;
}

#include "forces.hpp"

//---------------------------------------------------------------------------
void read_problem(
        std::vector< real2 > &points,
        std::vector< real2 > &velocity,
        std::vector< real  > &mass,
        std::vector< int   > &type,
        std::vector< std::array<unsigned, 2> > &conn,
        std::vector< unsigned                > &lock
        )
{
    size_t n;

    // Particles
    {
        std::string error_msg = "Error reading the particles file ("
            + config::coor_file + ")";

        std::ifstream f(config::coor_file);

        precondition(f, error_msg);

        precondition(f >> n, error_msg);

        points.clear(); points.reserve(n);
        mass.clear();   mass.reserve(n);
        type.clear();   type.reserve(n);

        for(size_t i = 0; i < n; ++i) {
            real2 p, v;
            real m;
            int t;

            precondition(f >> p.s[0] >> p.s[1] >> v.s[0] >> v.s[1] >> m >> t, error_msg);

            if (t == config::lock_type) {
                lock.push_back(points.size());
                t = 0;
            }

            points.push_back(p);
            velocity.push_back(v);
            mass.push_back(m);

            if (i == 0) {
                domain::lo = domain::hi = p;
            } else {
                domain::lo.s[0] = std::min(domain::lo.s[0], p.s[0]);
                domain::lo.s[1] = std::min(domain::lo.s[1], p.s[1]);

                domain::hi.s[0] = std::max(domain::hi.s[0], p.s[0]);
                domain::hi.s[1] = std::max(domain::hi.s[1], p.s[1]);
            }

            type.push_back(t);
        }
    }

    // Connections
    {
        std::string error_msg = "Error reading the connections file ("
            + config::conn_file + ")";

        std::ifstream f(config::conn_file);

        precondition(f, error_msg);

        size_t n;
        precondition(f >> n, error_msg);

        conn.clear(); conn.reserve(n);

        for(size_t k = 0; k < n; ++k) {
            std::array<unsigned, 2> c;
            precondition(f >> c[0] >> c[1], error_msg);
            conn.push_back(c);
        }
    }

    if (!config::init_file.empty()) {
        std::cout
            << "Reading initial conditions from \""
            << config::init_file << "\"" << std::endl;

        H5::H5File hdf(config::init_file, H5F_ACC_RDONLY);

        H5::DataType datatype = (std::is_same<real, float>::value ?
                H5::PredType::NATIVE_FLOAT : H5::PredType::NATIVE_DOUBLE);

        H5::DataSet time = hdf.openDataSet("time");

        hsize_t last_step;
        hdf.openDataSet("time").getSpace().getSimpleExtentDims(&last_step);

        H5::DataSet v, c;
        {
            std::ostringstream sname;
            sname << "step-" << last_step << "/velocity";
            v = hdf.openDataSet(sname.str());
        }
        {
            std::ostringstream sname;
            sname << "step-" << last_step << "/coord";
            c = hdf.openDataSet(sname.str());
        }

        hsize_t dim[2];
        H5::DataSpace fs = v.getSpace();
        precondition(fs.getSimpleExtentNdims() == 2, "Wrong format in input file");

        fs.getSimpleExtentDims(dim);
        precondition(dim[0] == n && dim[1] == 3,
                "Input file has wrong number of points");

        hsize_t count[] = {n, 2};
        hsize_t start[] = {0, 0};

        fs.selectHyperslab(H5S_SELECT_SET, count, start);

        dim[1] = 2;
        H5::DataSpace ms(2, dim);

        v.read(velocity.data(), datatype, ms, fs);
        c.read(points.data(), datatype);
    }
}

int main(int argc, char *argv[]) {
    namespace odeint = boost::numeric::odeint;

    try {
        config::read(argc, argv);

        vex::Context ctx(
                vex::Filter::Env             &&
                vex::Filter::DoublePrecision &&
                vex::Filter::Count(1)
                );
        std::cout << ctx << std::endl;

        vex::push_program_header(ctx,
                "typedef " + vex::type_name<real >() + " real;\n"
                "typedef " + vex::type_name<real2>() + " real2;\n"
                );

        std::vector< real2 > points;
        std::vector< real2 > velocity;
        std::vector< real  > mass;
        std::vector< int   > type;
        std::vector< std::array<unsigned, 2> > conn;
        std::vector< unsigned                > lock;

        read_problem(points, velocity, mass, type, conn, lock);

        real2 zero = {{0, 0}};
        std::vector< real2 > acc(points.size(), zero);

        vex::vector<real2>    p(ctx, points);
        vex::vector<real2>    v(ctx, velocity);
        vex::vector<real2>    a(ctx, acc);
        vex::vector<unsigned> lock_idx(ctx, lock);

        dem_system< real, forces<real> > S(ctx, points, mass, type, conn);

        odeint::velocity_verlet<
            vex::vector<real2>, // state
            vex::vector<real2>, // velocity
            real                // value
            > stepper;

        saver<real, real2> saver(config::save_file, type);

        boost::progress_display bar(config::tmax / config::tau);
        real tau, t = 0;
        bool locked = true;
        for(size_t step = 0; t < config::tmax; t += tau, ++bar, ++step) {
            if (t < config::tau)
                tau = config::tau / 100;
            else if (t < 5 * config::tau)
                tau = config::tau / 10;
            else
                tau = config::tau;

            stepper.do_step(std::ref(S), p, v, a, p, v, a, t, tau);

            if (config::freeze) S.freeze_outcasts(p, v);

            if (step % config::wstep == 0 && t > config::tunlock - 1) {
                vex::copy(p, points);
                vex::copy(v, velocity);
                vex::copy(a, acc);

                saver.add_step(t, points, velocity, acc);
            }

            if (locked && t >= config::tunlock) {
                VEX_FUNCTION(real2, unlock, (real2, p),
                        p.y = -1;
                        return p;
                        );

                vex::permutation(lock_idx)(p) = unlock(vex::permutation(lock_idx)(p));

                S.freeze_surface(p, v);

                locked = false;
            }

        }

    } catch (const vex::backend::error &err) {
        std::cerr << "Error: " << err << std::endl;
        return 1;
    } catch (const std::exception &err) {
        std::cerr << "Error: " << err.what() << std::endl;
        return 1;
    }
}
