#ifndef EDDY_FORCES_HPP
#define EDDY_FORCES_HPP

#include <string>
#include <sstream>
#include <fstream>
#include <boost/multi_array.hpp>
#include "config.hpp"
#include "precondition.hpp"

template <typename real>
class forces {
    public:
        typedef typename vex::cl_vector_of<real, 2>::type real2;

        VEX_FUNCTION_S(
                real2,
                local_force,
                (real2, pos)(real2, vel)(int, type)(real, mass)(real, time),
                local_force_body()
                );

        VEX_FUNCTION_SD(
                real2,
                potential_interaction,
                (real2, p1)(real2, p2)(int, type1)(int, type2),
                (lj),
                potential_interaction_body()
                );

    private:
        VEX_FUNCTION(real2, lj, (real2, x)(real2, y)(real, sigma)(real, eps),
                real2 d = x - y;
                real  r = length(d);
                if (r < 1e-8) return 0;

                real c  = sigma / r;
                real c3 = c * c * c;
                real c6 = c3 * c3;

                return -d * eps * c6 * (24 - 48 * c6) / (r * r);
            );


        static std::string local_force_body() {
            std::ostringstream src;

            // Modify the following line to set boundary force.
            src << "if (type == 0) return 0;\n";

            src << std::scientific << std::setprecision(8) <<
                "real2 a = " << -config::gamma << " * vel "
                " - mass * (real2)(0, " << config::grav << ");\n";

            // Wind forces:
            src << VEX_STRINGIZE_SOURCE(
                    if (pos.y > 95)
                        a += (real2)(10, 0);
                    else if (pos.y > 50)
                        a += (real2)(-10, 0);
                    else if (pos.y > 25)
                        a += (real2)(-5, 0);
                    );

            src << "return a;";

            return src.str();
        }

        static std::string potential_interaction_body() {
            // Read potentials.
            std::string error_msg = "Error reading the potential file ("
                + config::potential_file + ")";

            std::ifstream f(config::potential_file);
            precondition(f, error_msg);

            unsigned ntypes;
            precondition(f >> ntypes, error_msg);

            boost::multi_array<real, 2> sigma(boost::extents[ntypes][ntypes]);
            boost::multi_array<real, 2> eps  (boost::extents[ntypes][ntypes]);

            for(unsigned i = 0; i < ntypes; ++i)
                for(unsigned j = 0; j < ntypes; ++j)
                    precondition(f >> sigma[i][j], error_msg);

            for(unsigned i = 0; i < ntypes; ++i)
                for(unsigned j = 0; j < ntypes; ++j)
                    precondition(f >> eps[i][j], error_msg);

            std::ostringstream src;
            src << std::scientific << std::setprecision(8);

            src << "switch (type1) {\n";

            for(unsigned i = 0; i < ntypes; ++i) {
                src <<
                    "case " << i << ":\n"
                    "  switch (type2) {\n";
                for(unsigned j = 0; j < ntypes; ++j) {
                    src <<
                        "  case " << j << ": return lj(p1, p2, "
                        << sigma[i][j] << ", " << eps[i][j] << ");\n";
                }
                src << "  }\n";
            }
            src << "}\n";

            return src.str();
        }
};

#endif
