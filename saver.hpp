#ifndef SAVER_HPP
#define SAVER_HPP

#include <string>
#include <sstream>
#include <vector>
#include <array>
#include <type_traits>

#include <H5Cpp.h>
#include <pugixml.hpp>

template <typename real, typename point_type>
class saver {
    public:
        saver(const std::string &fname, const std::vector<int> &type)
            : hdfname(fname + ".h5"),
              xmfname(fname + ".xmf"),
              hdf(hdfname, H5F_ACC_TRUNC),
              step(0)
        {
            using namespace pugi;
            using namespace H5;

            if (std::is_same<real, float>::value)
                datatype = PredType::NATIVE_FLOAT;
            else
                datatype = PredType::NATIVE_DOUBLE;

            // XMF
            xmf.append_child(node_doctype).set_value("Xdmf SYSTEM \"Sdmf.dtd\" []");

            auto xdmf = xmf.append_child("Xdmf");
            xdmf.append_attribute("xmlns:xi").set_value("http://www.w3.org/2003/XInclude");
            xdmf.append_attribute("Version" ).set_value("2.2");

            auto domain = xdmf.append_child("Domain");

            time_collection = domain.append_child("Grid");
            time_collection.append_attribute("Name"          ).set_value("Time");
            time_collection.append_attribute("GridType"      ).set_value("Collection");
            time_collection.append_attribute("CollectionType").set_value("Temporal");

            // HDF
            {
                hsize_t zero  = 0;
                hsize_t unlim = H5S_UNLIMITED;
                DataSpace space(1, &zero, &unlim);

                hsize_t chunk_size = 1024;
                DSetCreatPropList p;
                p.setChunk(1, &chunk_size);

                time_ds = hdf.createDataSet("time", PredType::NATIVE_FLOAT, space, p);

                hsize_t n = type.size();

                hdf.createDataSet("type", PredType::NATIVE_INT, DataSpace(1, &n))
                    .write(type.data(), PredType::NATIVE_INT);
            }
        }


        void add_step(real time,
                const std::vector<point_type> &point,
                const std::vector<point_type> &velocity,
                const std::vector<point_type> &acceleration
                )
        {
            using namespace pugi;
            using namespace H5;

            ++step;

            {
                time_ds.extend(&step);

                hsize_t start = step - 1;
                hsize_t count = 1;

                DataSpace mspace = DataSpace(1, &count);
                DataSpace fspace = time_ds.getSpace();

                fspace.selectHyperslab(H5S_SELECT_SET, &count, &start);

                time_ds.write(&time, datatype, mspace, fspace);
            }

            std::string group;
            {
                std::ostringstream buf;
                buf << "step-" << step;
                group = buf.str();
            }

            std::string coo_name = group + "/coord";
            std::string vel_name = group + "/velocity";
            std::string acc_name = group + "/acceleration";

            hdf.createGroup(group);

            hsize_t dim[] = {point.size(), 2};
            hsize_t dimv[] = {point.size(), 3};

            hdf.createDataSet(
                    coo_name, PredType::NATIVE_FLOAT, DataSpace(2, dim)
                    ).write(
                        reinterpret_cast<const real*>(point.data()),
                        datatype
                        );

            {
                std::vector<real> buf(point.size() * 3, 0);
                for(size_t i = 0; i < point.size(); ++i) {
                    buf[i * 3 + 0] = velocity[i].s[0];
                    buf[i * 3 + 1] = velocity[i].s[1];
                }

                hdf.createDataSet(
                        vel_name, PredType::NATIVE_FLOAT, DataSpace(2, dimv)
                        ).write(buf.data(), datatype);
            }

            {
                std::vector<real> buf(point.size() * 3, 0);
                for(size_t i = 0; i < point.size(); ++i) {
                    buf[i * 3 + 0] = acceleration[i].s[0];
                    buf[i * 3 + 1] = acceleration[i].s[1];
                }

                hdf.createDataSet(
                        acc_name, PredType::NATIVE_FLOAT, DataSpace(2, dimv)
                        ).write(buf.data(), datatype);
            }

            hdf.flush(H5F_SCOPE_GLOBAL);

            auto grid = time_collection.append_child("Grid");
            grid.append_child("Time").append_attribute("Value").set_value(std::to_string(time).c_str());

            grid.append_attribute("Name"    ).set_value(group.c_str());
            grid.append_attribute("GridType").set_value("Uniform");

            auto topo = grid.append_child("Topology");
            topo.append_attribute("TopologyType"    ).set_value("Polyvertex");
            topo.append_attribute("NumberOfElements").set_value(std::to_string(dim[0]).c_str());

            auto geom = grid.append_child("Geometry");
            geom.append_attribute("GeometryType").set_value("XY");

            {
                auto data = geom.append_child("DataItem");
                data.append_attribute("Format"    ).set_value("HDF");
                data.append_attribute("NumberType").set_value("Float");
                data.append_attribute("Precision" ).set_value("4");
                data.append_attribute("Dimensions").set_value((std::to_string(dim[0]) + " 2").c_str());

                data.append_child(node_pcdata).set_value((hdfname + ":/" + coo_name).c_str());
            }

            {
                auto attr = grid.append_child("Attribute");
                attr.append_attribute("Name"         ).set_value("type");
                attr.append_attribute("AttributeType").set_value("Scalar");
                attr.append_attribute("Center"       ).set_value("Node");

                auto data = attr.append_child("DataItem");
                data.append_attribute("Dimensions").set_value(std::to_string(dim[0]).c_str());
                data.append_attribute("NumberType").set_value("Int");
                data.append_attribute("Precision" ).set_value("4");
                data.append_attribute("Format"    ).set_value("HDF");

                data.append_child(pugi::node_pcdata).set_value(
                        (hdfname + ":/type").c_str());
            }

            {
                auto attr = grid.append_child("Attribute");
                attr.append_attribute("Name"         ).set_value("velocity");
                attr.append_attribute("AttributeType").set_value("Vector");
                attr.append_attribute("Center"       ).set_value("Node");

                auto data = attr.append_child("DataItem");
                data.append_attribute("Dimensions").set_value((std::to_string(dim[0]) + " 3").c_str());
                data.append_attribute("NumberType").set_value("Float");
                data.append_attribute("Precision" ).set_value("4");
                data.append_attribute("Format"    ).set_value("HDF");

                data.append_child(pugi::node_pcdata).set_value(
                        (hdfname + ":/" + vel_name).c_str());
            }

            {
                auto attr = grid.append_child("Attribute");
                attr.append_attribute("Name"         ).set_value("acceleration");
                attr.append_attribute("AttributeType").set_value("Vector");
                attr.append_attribute("Center"       ).set_value("Node");

                auto data = attr.append_child("DataItem");
                data.append_attribute("Dimensions").set_value((std::to_string(dim[0]) + " 3").c_str());
                data.append_attribute("NumberType").set_value("Float");
                data.append_attribute("Precision" ).set_value("4");
                data.append_attribute("Format"    ).set_value("HDF");

                data.append_child(pugi::node_pcdata).set_value(
                        (hdfname + ":/" + acc_name).c_str());
            }

            xmf.save_file(xmfname.c_str(), "  ");
        }
    private:
        std::string hdfname;
        std::string xmfname;

        H5::H5File  hdf;
        H5::DataSet time_ds;

        H5::DataType datatype;

        pugi::xml_document xmf;
        pugi::xml_node     time_collection;

        hsize_t step;
};

#endif
