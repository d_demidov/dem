#ifndef PRECONDITION_HPP
#define PRECONDITION_HPP

#include <stdexcept>

template <class Cond>
void precondition(Cond &&cond, const std::string &message) {
    if (!static_cast<bool>(cond)) throw std::runtime_error(message);
}

#endif
