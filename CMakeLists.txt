cmake_minimum_required(VERSION 2.8)
project(dem)

add_definitions(-DVEXCL_CACHE_KERNELS -DBOOST_PROTO_MAX_ARITY=15)

#----------------------------------------------------------------------------
# Precision
#----------------------------------------------------------------------------
option(SINGLE_PRECISION "Use single precision" OFF)
if (SINGLE_PRECISION)
    add_definitions(-DSINGLE_PRECISION)
endif()

#----------------------------------------------------------------------------
# Find OpenCL
#----------------------------------------------------------------------------
set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)
find_package(OpenCL REQUIRED)
include_directories( ${OPENCL_INCLUDE_DIRS} )

#----------------------------------------------------------------------------
# Find VexCL
#----------------------------------------------------------------------------
set(VEXCL_ROOT $ENV{VEXCL_ROOT} CACHE STRING "VexCL root")
include_directories( ${VEXCL_ROOT} )

#----------------------------------------------------------------------------
# Find odeint
#----------------------------------------------------------------------------
#set(ODEINT_ROOT $ENV{ODEINT_ROOT} CACHE STRING "odeint root")
#include_directories( ${ODEINT_ROOT} )

#----------------------------------------------------------------------------
# Find Boost
#----------------------------------------------------------------------------
set(BOOST_COMPONENTS
    date_time
    filesystem
    program_options
    system
    )
find_package(Boost COMPONENTS ${BOOST_COMPONENTS})
include_directories( ${Boost_INCLUDE_DIRS} )

#----------------------------------------------------------------------------
# Enable C++11 support, set compilation flags
#----------------------------------------------------------------------------
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x -Wall -Wclobbered -Wempty-body -Wignored-qualifiers -Wmissing-field-initializers -Wsign-compare -Wtype-limits -Wuninitialized -Wunused-parameter -Wunused-but-set-parameter -Wno-comment -Wno-type-limits -Wno-strict-aliasing")
endif ()

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x -Wall -Wempty-body -Wignored-qualifiers -Wmissing-field-initializers -Wsign-compare -Wtype-limits -Wuninitialized -Wunused-parameter -Wno-comment -Wno-tautological-compare")

    option(USE_LIBCPP "Use libc++ with Clang" OFF)
    if (USE_LIBCPP)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++")
    endif ()
endif ()

#----------------------------------------------------------------------------
include_directories(${CMAKE_CURRENT_SOURCE_DIR})

function(add_dem_exec TARGET FORCES)
    add_executable(${TARGET} dem.cpp config.cpp)

    target_include_directories(${TARGET} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/${FORCES})

    target_link_libraries(${TARGET}
        ${Boost_LIBRARIES}
        ${OPENCL_LIBRARIES}
        hdf5 hdf5_cpp
        pugixml
        )
endfunction()

add_dem_exec(dem      default)
add_dem_exec(verbatim verbatim)
add_dem_exec(eddy     eddy)
add_dem_exec(shake    shake)
