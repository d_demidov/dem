#!/usr/bin/python
from sys import argv
from random import randint

hb = 0.5
hm = 0.75
hw = 0.1

if len(argv) < 2:
    L = 128;
else:
    L = int(argv[1]);

Hw = 1.25 * L
Hl = 0.25 * L

part = []
conn = []

#---------------------------------------------------------------------------
# Walls
#---------------------------------------------------------------------------

x = 0
y = Hw

part.append( (x, y, 1, 0) )

while y >= 0:
    y = y - hw
    part.append( (x, y, 1, 0) )

while x <= L:
    x = x + hw
    part.append( (x, y, 1, 0) )

while y <= Hw:
    y = y + hw
    part.append( (x, y, 1, 0) )

#---------------------------------------------------------------------------
# Medium
#---------------------------------------------------------------------------
nx = int(L / hm) - 1
mx = 10

for j in range(1, int(2 * nx / 3)):
    for i in range(1, nx):
        part.append( (i * hm, Hl + j * hm, 100, 1) )

#---------------------------------------------------------------------------
# Lock
#---------------------------------------------------------------------------
x = L / 2
y = Hw + 10 - 2 * hb

for i in range(-2, mx + 3):
    part.append( (x + i * hb, y, 1, 3) )

for j in range(1, 5):
    part.append( (x - 2 * hb, y + j * hb, 1, 3) )
    part.append( (x + (mx + 2) * hb, y + j * hb, 1, 3) )


x = 0
y = Hl
w = 10

while x <= w:
    x = x + hw
    part.append( (x, y, 1, 0) )

while x <= L - w:
    x = x + hw
    part.append( (x, y, 1, 3) )

while x <= L:
    x = x + hw
    part.append( (x, y, 1, 0) )

#---------------------------------------------------------------------------
# The big object
#---------------------------------------------------------------------------
x0 = L / 2
y0 = Hw + 10

idx = len(part)
for j in range(0, mx):
    for i in range(0, mx):
        part.append( (x0 + i * hb, y0 + j * hb, 50, 2) )

        if i > 0:
            conn.append( (idx, idx - 1) )

        if j > 0:
            conn.append( (idx, idx - mx) )

        if i > 0 and j > 0:
            conn.append( (idx, idx - 1 - mx) )

        if i < mx - 1 and j > 0:
            conn.append( (idx, idx + 1 - mx) )

        idx = idx + 1

pf = open('part.txt', 'w')
pf.write("%s\n" % len(part))

for p in part:
    pf.write("%s %s 0 0 %s %s\n" % p)

cf = open('conn.txt', 'w')
cf.write("%s\n" % len(conn))

for c in conn:
    cf.write("%s %s\n" % c)

open("dem.cfg", "w").write(
"""
tau = 1e-4
wstep = 1000
tunlock = 20
lock = 3
""")


#---------------------------------------------------------------------------
# Potentials
#---------------------------------------------------------------------------
class zero:
    def write(self, f):
        f.write("return 0;\n")

class lj:
    def __init__(self, sigma, eps):
        self.sigma = sigma
        self.eps   = eps

    def write(self, f):
        f.write(
            "real2 d = p1 - p2; "
            "real  r = length(d); "
            "if (r < 1e-8) return 0; "
            "real c  = %s / r; "
            "real c3 = c * c * c; "
            "real c6 = c3 * c3; "
            "return d * %s * c6 * (48 * c6 - 24) / (r * r);\n" % (
                self.sigma, self.eps)
            )

class zig1:
    def __init__(self, sigma, eps):
        self.sigma = sigma
        self.eps   = eps

    def write(self, f):
        f.write(
            "real2 d = p1 - p2; "
            "real  r = length(d); "
            "if (r < 1e-8) return 0; "
            "real c  = 1.45 * %s / r; "
            "real c2 = c * c; "
            "real c3 = c2 * c; "
            "real c6 = c3 * c3; "
            "return d * %s * c6 * (1.8 * c6 - 0.6 * c3 + 0.225 * c2) / (r * r);\n" % (
                self.sigma, self.eps)
            )

class zig2:
    def __init__(self, sigma, eps):
        self.sigma = sigma
        self.eps   = eps

    def write(self, f):
        f.write(
            "real2 d = p1 - p2; "
            "real  r = length(d); "
            "if (r < 1e-8) return 0; "
            "real c  = 1.28 * %s / r; "
            "real c2 = c * c; "
            "real c3 = c2 * c; "
            "real c6 = c3 * c3; "
            "return d * %s * c6 * (12 * c6 - 36 * c3 + 14.4 * c2 + 6) / (r * r);\n" % (
                self.sigma, self.eps)
            )

potentials = [
        [zero(),       zero(),       zero()],
        [lj(1.0, 0.1), zig2(0.4, 1e5), lj(1.0, 0.1)],
        [lj(1.0, 0.1), lj(1.0, 0.1),   lj(1.0, 0.1)]
        ]

n = len(potentials)
f = open('potential.txt', 'w')
f.write("%s\n" % n)

for row in range(0, n):
    for col in range(0, n):
        potentials[row][col].write(f)

