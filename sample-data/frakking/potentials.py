#!/usr/bin/python

class zero:
    def write(self, f):
        f.write("return 0;\n")

class lj:
    def __init__(self, sigma, eps):
        self.sigma = sigma
        self.eps   = eps

    def write(self, f):
        f.write(
            "real2 d = p1 - p2; "
            "real  r = length(d); "
            "if (r < 1e-8) return 0; "
            "real c  = %s / r; "
            "real c3 = c * c * c; "
            "real c6 = c3 * c3; "
            "return d * %s * c6 * (48 * c6 - 24) / (r * r);\n" % (
                self.sigma, self.eps)
            )

class zig:
    def __init__(self, sigma, eps):
        self.sigma = sigma
        self.eps   = eps

    def write(self, f):
        f.write(
            "real2 d = p1 - p2; "
            "real  r = length(d); "
            "if (r < 1e-8) return 0; "
            "real c  = 1.36 * %s / r; "
            "real c2 = c * c; "
            "real c3 = c2 * c; "
            "real c6 = c3 * c3; "
            "return d * %s * c6 * (4.8 * c6 - 14.4 * c3 + 7.2 * c2 + 2.4) / (r * r);\n" % (
                self.sigma, self.eps)
            )

potentials = [
        [zero(),       zero(),       zero()],
        [zig(1.0, 0.1), zig(2.0, 1e4), zig(2.0, 1e4)],
        [zig(1.0, 0.1), zig(2.0, 1e4), lj(0.5, 0.1)]
        ]

n = len(potentials)
f = open('verbatim.txt', 'w')
f.write("%s\n" % n)

for row in range(0, n):
    for col in range(0, n):
        potentials[row][col].write(f)
